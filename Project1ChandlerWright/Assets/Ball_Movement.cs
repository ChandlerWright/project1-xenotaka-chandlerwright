﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Ball_Movement : MonoBehaviour {

	public float baseSpeed = 2;
	public float maxSpeed = 9;
	public float currentSpeed;
	public Vector3 movementVector;
	int leftScore, rightScore = 0;
	public Text leftBoard;
	public Text rightBoard;
	public string nextScene = "";

	// Use this for initialization
	void Start () {
		leftBoard.text = "" + leftScore;
		rightBoard.text = "" + rightScore;
		movementVector.y = InitialRotation ();
		movementVector.x = TossUp ();
		currentSpeed = baseSpeed;
	}
	
	// Update is called once per frame
	void Update () {
		Movement ();
		SceneChanger ();
	}

	public void SceneChanger(){
		if (leftScore > 5 || rightScore > 5) {
			SceneManager.LoadScene (nextScene);
		}
	}

	public int TossUp(){
		int minX = -1;
		int maxX = 2;
		int direction = 0;
		while (direction == 0) {
			direction = (Random.Range (minX, maxX));
		}
		return direction;
	}
		
	public float InitialRotation(){
		float minZ = -0.8f;
		float maxZ = 0.8f;

		return(Random.Range(minZ, maxZ));
	}

	public void Movement(){
		transform.Translate (movementVector * currentSpeed * Time.deltaTime);
	}

	public void OnTriggerEnter(Collider other){
		if (other.transform.CompareTag ("Paddle")) {
			movementVector.x *= -1;
			currentSpeed = Mathf.Min (currentSpeed + 0.5f, maxSpeed);
		} else if (other.transform.CompareTag ("Border")) {
			movementVector.y *= -1;
		} else if (other.transform.CompareTag ("ScoreL")) {
			rightScore++;
			rightBoard.text = "" + rightScore;
			transform.position = Vector3.zero;
			movementVector.y = InitialRotation ();
			movementVector.x = TossUp ();
			currentSpeed = baseSpeed;
		} else if (other.transform.CompareTag ("ScoreR")) {
			leftScore++;
			leftBoard.text = "" + leftScore;
			transform.position = Vector3.zero;
			movementVector.y = InitialRotation ();
			movementVector.x = TossUp ();
			currentSpeed = baseSpeed;
		}
	}
}
