﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle_Movement : MonoBehaviour {

	public float movementSpeed = 5f;
	public bool multiplayer = true;
	public GameObject playerOne;
	public GameObject playerTwo;
	public GameObject ball;

	public float minOffsetChange = -0.05f;
	public float maxOffsetChange = 0.05f;
	public float minOffset = -3f;
	public float maxOffset = 3f;
	public float offsetChange = 0f;
	public float AIOffset = 0f;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("OffsetCalc", 2f, 2f);
	}
	
	// Update is called once per frame
	void Update () {
		POneMovement ();
		PTwoMovement ();
		AIOffset += offsetChange;
		if (AIOffset > maxOffset) {
			AIOffset = maxOffset;
		}
		if (AIOffset < minOffset) {
			AIOffset = minOffset;
		}
	}

	public void POneMovement(){
		if (Input.GetKey (KeyCode.W)) {
			playerOne.transform.Translate (Vector3.up * movementSpeed * Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.S)) {
			playerOne.transform.Translate (Vector3.down * movementSpeed * Time.deltaTime);
		}
		if (playerOne.transform.position.y > 3.5f) {
			playerOne.transform.position = new Vector3 (playerOne.transform.position.x, 3.5f, playerOne.transform.position.z);
		}
		if (playerOne.transform.position.y < -3.5f) {
			playerOne.transform.position = new Vector3(playerOne.transform.position.x, -3.5f, playerOne.transform.position.z);
		}
	}

	public void PTwoMovement(){
		if (multiplayer) {
			if (Input.GetKey (KeyCode.UpArrow)) {
				playerTwo.transform.Translate (Vector3.up * movementSpeed * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.DownArrow)) {
				playerTwo.transform.Translate (Vector3.down * movementSpeed * Time.deltaTime);
			}
		} else {
			playerTwo.transform.position = (new Vector3(playerTwo.transform.position.x, ball.transform.position.y + AIOffset, playerTwo.transform.position.z));
		}
		if (playerTwo.transform.position.y > 3.5f) {
			playerTwo.transform.position = new Vector3 (playerTwo.transform.position.x, 3.5f, playerTwo.transform.position.z);
		}
		if (playerTwo.transform.position.y < -3.5f) {
			playerTwo.transform.position = new Vector3(playerTwo.transform.position.x, -3.5f, playerTwo.transform.position.z);
		}
	}

	public void OffsetCalc(){
		offsetChange = Random.Range (minOffsetChange, maxOffsetChange);
	}
}
